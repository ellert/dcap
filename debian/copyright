Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: dcap
Upstream-Contact: Tigran Mkrtchyan <tigran.mkrtchyan@desy.de>
Source: https://github.com/dCache/dcap/archive/2.47.14.tar.gz

Files: *
Copyright: © 2000-2004 DESY Hamburg DMG-Division
License: LGPL-2

Files: plugins/gssapi/base64.? plugins/gssapi/util.c
Copyright: © 1997-2000 Kungliga Tekniska Högskolan (Royal Institute of
                       Technology), Stockholm, Sweden
License: BSD-3-Clause

Files: plugins/gssapi/gssIoTunnel.c
Copyright: © 2000, 2001, 2002 DESY Hamburg DMG-Division
           © 1997-2002 Kungliga Tekniska Högskolan (Royal Institute of
                       Technology), Stockholm, Sweden
License: LGPL-2

Files: src/dcap_unix2win.c
Copyright: © 1994 Jason R. Thorpe. All rights reserved.
License: BSD-3-Clause

Files: debian/*
Copyright: © 2009-2024 Mattias Ellert <mattias.ellert@physics.uu.se>
License: LGPL-2

License: LGPL-2
 On Debian systems the full text of the LGPL-2 license can be found
 in the /usr/share/common-licenses/LGPL-2 file.

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 3. Neither the name of the Institute nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.
