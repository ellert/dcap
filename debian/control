Source: dcap
Priority: optional
Maintainer: Mattias Ellert <mattias.ellert@physics.uu.se>
Build-Depends:
 debhelper-compat (= 13),
 dpkg-dev (>= 1.22.5),
 libglobus-gssapi-gsi-dev,
 libkrb5-dev,
 libssl-dev,
 zlib1g-dev,
 libcunit1-dev
Standards-Version: 4.6.2
Section: libs
Vcs-Browser: https://salsa.debian.org/ellert/dcap
Vcs-Git: https://salsa.debian.org/ellert/dcap.git
Homepage: https://www.dcache.org/manuals/libdcap.shtml

Package: dcap
Section: net
Architecture: any
Multi-Arch: foreign
Depends:
 libdcap1t64 (= ${binary:Version}),
 ${shlibs:Depends},
 ${misc:Depends}
Description: Client Tools for dCache
 dCache is a distributed mass storage system.
 This package contains the client tools.

Package: libdcap1t64
Provides: ${t64:Provides}
Replaces: libdcap1
Breaks: libdcap1 (<< ${source:Version})
Architecture: any
Multi-Arch: same
Depends:
 ${shlibs:Depends},
 ${misc:Depends}
Pre-Depends:
 ${misc:Pre-Depends}
Suggests:
 dcap-tunnel-gsi (= ${binary:Version}),
 dcap-tunnel-krb (= ${binary:Version}),
 dcap-tunnel-ssl (= ${binary:Version}),
 dcap-tunnel-telnet (= ${binary:Version})
Description: Client Libraries for dCache
 dCache is a distributed mass storage system.
 This package contains the client libraries.

Package: dcap-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 libdcap1t64 (= ${binary:Version}),
 ${misc:Depends}
Description: Client Development Files for dCache
 dCache is a distributed mass storage system.
 This package contains the client development files.

Package: dcap-tunnel-gsi
Architecture: any
Multi-Arch: same
Depends:
 libdcap1t64 (= ${binary:Version}),
 ${shlibs:Depends},
 ${misc:Depends}
Pre-Depends:
 ${misc:Pre-Depends}
Description: GSI tunnel for dCache
 This package contains the gsi tunnel plugin library used by dcap.
 This library is dynamically loaded at runtime.

Package: dcap-tunnel-krb
Architecture: any
Multi-Arch: same
Depends:
 libdcap1t64 (= ${binary:Version}),
 ${shlibs:Depends},
 ${misc:Depends}
Pre-Depends:
 ${misc:Pre-Depends}
Description: Kerberos tunnel for dCache
 This package contains the kerberos tunnel plugin library used by dcap.
 This library is dynamically loaded at runtime.

Package: dcap-tunnel-ssl
Architecture: any
Multi-Arch: same
Depends:
 libdcap1t64 (= ${binary:Version}),
 ${shlibs:Depends},
 ${misc:Depends}
Pre-Depends:
 ${misc:Pre-Depends}
Description: SSL tunnel for dCache
 This package contains the ssl tunnel plugin library used by dcap.
 This library is dynamically loaded at runtime.

Package: dcap-tunnel-telnet
Architecture: any
Multi-Arch: same
Depends:
 libdcap1t64 (= ${binary:Version}),
 ${shlibs:Depends},
 ${misc:Depends}
Pre-Depends:
 ${misc:Pre-Depends}
Description: Telnet tunnel for dCache
 This package contains the telnet tunnel plugin library used by dcap.
 This library is dynamically loaded at runtime.
